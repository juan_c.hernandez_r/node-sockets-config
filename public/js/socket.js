var socket = io();

//   Escuchar informacion = on
socket.on("connect", function () {
  console.log("Conectado al servidor");
});

//   Verificar si nos desconectamos del servidor
socket.on("disconnect", function () {
  console.log("Perdimos conexion con el servidor");
});

//   Enviar informacion = emit
socket.emit(
  "enviarMensaje",
  {
    usuario: "Camilo",
    mensaje: "Hola mundo",
  },
  function (res) {
    console.log("Respuesta server", res);
  }
);

//   Escuchar informacion
socket.on("enviarMensaje", function (data) {
  console.log("Servidor", data);
});
